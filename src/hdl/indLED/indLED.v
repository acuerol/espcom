////////////////////////////////////////////////////////////////////////////
// Authors: Alexis Cuero Losada
//	Revision: 1
// Description: Control for led indicator.
// Date: 2015-07-16
////////////////////////////////////////////////////////////////////////////

module indLED(clk, rst, addr, cs, rd, wr, data_in, data_out, led_out);

	//-------------------- Inputs
	input clk;
	input rst;
	input [3:0]addr;
	input cs;
	input rd;
	input wr;
	input [5:0]data_in;
	
	//-------------------- Outputs
	output reg [5:0]data_out;
	output reg [5:0]led_out;
	
	reg [3:0]sel_mux;
	wire [5:0]led_data_out;
	
	always @(*) begin
		case(addr)
			4'h0: sel_mux = (cs & wr) ? 4'h1 : 4'h0; // Set data in.
			4'h2: sel_mux = (cs & rd) ? 4'h2 : 4'h0; // Get data out.
			default: sel_mux = 0;
		endcase
	end
	
	always @(negedge clk) begin
		led_out = sel_mux[0] ? data_in : led_out;
		data_out = sel_mux[1] ? led_out : data_out;
		
		if(rst) begin
			led_out = 0;
		end
	end
endmodule
