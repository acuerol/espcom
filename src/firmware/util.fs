\ Author: Alexis Cuero Losada
\ Email: alcuerol@gmail.com
\ Date: 2015-07-21
\ Version: 1.0

h# 64 constant received_uart
variable a
variable b
variable l_A
variable l_B
variable time

\ --------------------util definitions--------------------

\ Return the multiplication of n1 and n2 in two parts n3 ( msb ) and n4 ( lsb ).
: multiplicar ( n1 n2 -- n3n4 = n1*n2 )
	multi_b !
	multi_a !
	d# 1 multi_init !
	begin multi_done @ d# 1 = until
	multi_pp_high @
	multi_pp_low @
;

: dividir
	swap div_a !
	div_b !
	d# 1 div_init !
	begin div_done @ d# 1 = until
	div_c @
;

\ Returns the n-th power of b.
: pow ( b n -- b^n )
	dup 0> if													( b n )
		over 0= if												( b n b ) ( b n )
			drop													( b )
		else														( b n )
			h# 1 -rot swap										( b n res ) ( res b n ) ( res n b )
			begin
				rot over multiplicar nip					( n b res ) ( n b res b ) ( n b {B, A} ) ( n b res*b )
				rot 1- rot over								( b res*b n ) ( b res*b n-1 ) ( res*b n-1 b ) ( res*b n-1 b n-1 )
			0= until												( res*b n-1 b )
			2drop
		then
	else															( b n )
		h# 1
	then
;

\ -------------------------------indLED
\ Updates the LEDs state, put the indLED output = LED_on_list.
: refresh_LED ( LED_on_list -- )
	LED_write !
;

\ Reads the LEDs state.
: read_LED ( -- LED_on_list )
	LED_read @
;

\ Checks the nth LED state.
: check-led ( n -- n LED[n] )
	d# 2 over pow								( n 2 ) ( n 2 n ) ( n 2^n )
	d# 33 < if									( n 2^n 33 ) ( n 2^n<33 ) ( n )
		read_LED									( n LED )
		over rshift								( n LED>>[n-1] )
		d# 2 mod 0= if							( n LED>>[n-1]mod2=0 )
			d# 0
		else
			d# 1
		then
	then
;

\ Turns on the nth LED.
: on-led ( n -- )
	dup h# 0 >= over h# 6 < 
		and if
		check-led						( n LED[n] )
		0= if								( n LED[n] 0 ) ( n LED[n]=0 ) ( n )
			d# 2 swap pow 				( n 2 ) ( 2 n ) ( 2^n )
			read_LED swap +			( 2^n LED ) ( LED 2^n ) ( LED+2^n )
			refresh_LED
		else
			drop
		then
	then
;

\ Turns off the nth LED.
: off-led ( n -- )
	dup h# 0 >= over h# 6 < 
	and if
		check-led						( n LED[n] )
		d# 1 = if								( n LED[n] 0 ) ( n LED[n]=0 ) ( n )
			d# 2 swap pow 				( n 2 ) ( 2 n ) ( 2^n )
			read_LED swap -			( 2^n LED ) ( LED 2^n ) ( LED-2^n )
			refresh_LED
		else
			drop
		then
	then
;

\ Turns on all LEDs.
: on-leds ( n -- )
	h# 3F refresh_LED
;

\ Turns off all LEDs.
: off-leds ( n -- )
	h# 0 refresh_LED
;

\ --------------------strRAM definitions--------------------
\ Stores a data in a strRAM addres.
: save-char ( data addr -- )
	swap strRAM_write !
	strRAM_set_addr !
	d# 1 strRAM_init !
;

\ Loads a data from strRAM addres.
: load-char ( addr -- char )
	strRAM_set_addr !
	strRAM_init @ drop
	strRAM_read @
;

\ Converts a native string to string in strRAM.
: str-to-RAM ( a l{A} B -- flag )
	over															( a l{A} B l{A} )
	0> if															( a l{A} B )
		2dup save-char 1+ b ! swap a !					( a l{A} B l{A} B ) ( a l{A} b ) ( l{A} a ) ( l{A} )
		begin														( c )
			1- dup a get + c@									( c-- c-- a ) ( c-- a> ) ( c-- c{a>} )
			over b get + save-char dup						( c-- c{a>} c-- b ) ( c-- c{a>} b> ) ( c-- c-- )
			0= if													( c-- )
				drop h# 1
			else
				h# 2
			then
		h# 1 = until
	else
		drop 2drop
	then
;

\ Sets zero to the length of the received_uart string.
: clean-received-uart ( -- )
	h# 0 received_uart save-char
;

\ --------------------RAM definitions--------------------

\ Stores a data in RAM addr.
\ [15:0]data [7:0]addr
: save ( data addr -- )
	swap RAM_write !
	RAM_set_addr !
	h# 1 RAM_init !
;

\ Loads a data from an RAM addr.
\ [7:0]addr
: load ( addr -- data )
	RAM_set_addr !
	RAM_init @ drop
	RAM_read @
;

\ --------------------espDriver definitions--------------------
\ Resets the ESP8266.
: reset-module ( -- )
	d# 1 module_rst !
;

\ --------------------Timer definitions--------------------
\ Count n miliseconds.
\ [15:0]n
: time-count ( n -- )
	h# 1 timer_rst !
	begin
		dup timer_cycles @
	< until
	drop
;

\ --------------------UART definitions--------------------
\ Puts the UART in listen mode for time miliseconds.
: listen-uart ( time -- uart_out flag flag )
	begin																		( t )
		uart_rx_busy @ 0=
		uart_tx_busy @ 0=
	and until
	h# 1 uart_rx_init !
	h# 1 timer_rst !
	begin																		( t )
		dup timer_cycles @												( t t timer_cycles ) ( t t<time_cycles )
		< if																	( t )
			drop h# 0 dup dup												( 0 0 0 )
		else																	( t )
			uart_done @														( t uart_done 1 )
			h# 1 = if														( t )
				drop uart_read @ h# 1 h# 1
			else																( t )
				h# 2
			then
		then
	h# 2 <> until
;

\ --> Reescribir <-- Escucha datos de la UART por 1s indefinidamente, se detiene cuando el dato recibido es cero.
: listen-and-save ( time -- )
	clean-received-uart
	h# 0 begin																( t c )
		over listen-uart													( t c t ) ( c data flag )
		h# 1 = if															( t c data )
			swap 1+ swap over 											( t c++ data ) ( t c++ data c++ )
			dup received_uart save-char								( t c++ data c++ c++ received-data ) ( t c++ data c++ )
			received_uart + save-char									( t c++ data received-data+c++ ) ( t c++ )
			h# 2
		else																	( t c data )
			drop 2drop h# 0
		then
	h# 2 <> until
;

\ Emits a string through the UART.
: type-uart ( addr length -- )
	d# 0 do
		dup c@ emit-uart
		1+
	loop
	drop
;

\ Emits a string store in strRAM through UART.
: type-str-uart ( A -- )
	dup load-char l_A ! 1+									( A l{A} ) ( a )
	h# 0 begin													( a c )
		2dup + load-char emit-uart 1+						( a c a> ) ( a c c{a>} ) ( a c++ )
		dup l_A get												( a c++ c++ l{A} )
		>= if														( a c++ )
			2drop h# 1
		else
			h# 2
		then
	h# 2 <> until
;
