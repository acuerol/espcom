include util_str.fs
include util_ESP.fs

variable led

: target

	reset-module
	d# 1500 time-count
	
	s" AT" type-to-ESP
	d# 10 listen-and-save
	check-ok
	h# 1 = if																			( AT )
		h# 00 h# 3 build-scwmode
		h# 00 send-to-ESP
		d# 10 listen-and-save
		check-ok
		h# 1 = if																		( CWMODE )
			h# 00 s" UNE_AD96" s" 00986252202208" build-scwjap
			h# 00 send-to-ESP
			d# 5000 listen-and-save
			check-ok
			h# 1 = if																	( CWJAP )
				h# 0 on-led
			else
				h# 5 on-led
			then
		else
			h# 5 on-led
		then
	else
		h# 5 on-led
	then
;

: config-base
	reset-module															( reset )
	d# 1500 time-count
	
	s" AT" type-to-ESP
	d# 1000 listen-and-save
	check-ok
	h# 1 = if																( AT )
		h# 00 h# 1 build-scwmode
		h# 00 send-to-ESP
		d# 1000 listen-and-save
		check-ok
		h# 1 = if															( CWMODE )
			h# 00 s" 192.168.0.100" build-scipsta
			h# 00 send-to-ESP
			d# 1000 listen-and-save
			build-scipsta
			
			h# 00 h# 1 build-scipmux
			h# 00 send-to-ESP
			d# 1500 listen-and-save
			check-ok
			h# 1 = if														( CIPMUX )
				h# 00 s" 8080" build-scipserver-create
				h# 00 send-to-ESP
				d# 1000 listen-and-save
				check-ok
				h# 1 = if
					h# 00 s" CLIENT" s" 1234" build-scwjap
					h# 00 send-to-ESP
					d# 5000 listen-and-save
					check-ok
					h# 1 = if
						h# 0 on-led
					else
						h# 5 on-led
					then
				else
					h# 5 on-led
				then
			else
				h# 5 on-led
			then
		else
			h# 5 on-led
		then
	else
		h# 5 on-led
	then
;

: blink
	on-leds
	d# 100 time-count
	off-leds
	d# 100 time-count
	on-leds
	d# 100 time-count
	off-leds
	d# 100 time-count
;

: main
	drop ( <-- There is a 10 in the stack. )
	
	h# 0 begin
		blink
		d# 5000 listen-and-save
		received_uart send-to-ESP
	1+ again
;
