\ Author: Alexis Cuero Losada
\ Email: alcuerol@gmail.com
\ Date: 2015-07-23
\ Version: 1.0

variable a
variable b
variable c
variable d
variable l_A
variable l_B
variable l_C
variable l_D

\ Emits 0d and 0a through the UART.
: emit-cmd-end
	h# 0d emit-uart h# 0a emit-uart
;

\ Emits a string through the UART with 0d and 0a at the end.
: send-to-ESP ( A -- )
	type-str-uart
	emit-cmd-end
;

: type-to-ESP ( A -- )
	type-uart
	emit-cmd-end
;

: check-ok ( -- flag )
	received_uart s" OK" contains
;

\ Builds a string for set the module mode.
\ 1 - Station Mode. 2 - AP Mode. 3 - AP + Station Mode.
: build-scwmode ( addr mode -- )
	num2char
	over s" AT+CWMODE=" rot str-to-RAM								( A mode A b l{B} ) ( A mode b l{B} A ) ( A mode )
	swap put-char															( mode A ) (  )
;

\ Builds a string for ask the module mode.
\ Response: +CWMODE:<mode> OK
: build-qcwmode ( addr -- )
	s" AT+CWMODE?" rot str-to-RAM										( A b l{B} )
;

\ Builds a string for set the AP to connect.
: build-scwjap ( A ssid l{ssid} pwd l{pwd} -- )
	l_C ! c ! l_B ! b ! a ! 
	s" AT+CWJAP=" a get str-to-RAM									( str l{str} A ) (  )
	h# 22 A get put-char													( " )
	b get l_B get a get put-at-end									( b l{B} A ) (  )
	h# 22 A get put-char h# 2C A get put-char						( " , )
	h# 22 A get put-char													( " )
	c get l_C get a get put-at-end									( c l{C} A ) (  )
	h# 22 A get put-char													( " )
;

\ Builts a string for ask the AP ssid which the module is connected.
\ Response: +CWJAP:<ssid> OK
: build-qcwjap ( addr --  )
	s" AT+CWJAP?" rot str-to-RAM										( A b l{B} )
;

\ Builts a string for list all available APs with ssid = "ssid".
: build-scwlap ( addr ssid l{ssid} -- )
	l_A ! a !
	dup s" AT+CWLAP=" rot str-to-RAM									( A A b l{B} ) ( A b l{B} A )
	h# 22 over put-char												( " )
	dup a get l_A get rot put-at-end
	h# 22 swap put-char													( " )
;

\ Builts a string for list all available APs.
: build-ecwlap ( addr -- )
	s" AT+CWLAP" rot str-to-RAM										( A b l{B} )
;

\ Builts a string for disconnect from AP.
: build-ecwqap ( addr -- )
	s" AT+CWQAP" rot str-to-RAM										( A b l{B} )
;

\ Builts a string for ask the softAP configuration.
: build-qcwsap ( addr -- )
	s" AT+CWSAP?" rot str-to-RAM										( A b l{B} )
;

: build-scwsap ( addr ssid l{ssid} pwd l{pwd} chl ecn -- )
	d ! c ! l_B ! b ! l_A ! a !
	dup s" AT+CWSAP=" rot str-to-RAM									( A A b l{B} ) ( A )
	h# 22 over put-char													( " )
	dup a get l_A get rot put-at-end									( A A ssid l{ssid} ) ( A ssid l{ssid} A ) ( A )
	h# 22 over put-char h# 2C over put-char						( " , )
	h# 22 over put-char													( " )
	dup b get l_B get rot put-at-end									( A A pwd l{pwd} ) ( A pwd l{pwd} A ) ( A )
	h# 22 over put-char h# 2C over put-char						( " , )
	c get num2char over put-char										( A chl A ) ( A )
	h# 2C over put-char													( , )
	d get num2char swap put-char										( A ecn A ) ( A )
;

\ Builts a string for get the IP of statins which are connected to ESP8266.
: build-ecwlif ( addr -- )
	s" AT+CWLIF" rot str-to-RAM
;

\ Builts a string for ask the IP of the station.
: build-qcipsta ( addr -- )
	s" AT+CIPSTA?" rot str-to-RAM
;

\ Builts a string for set the IP of the station.
: build-scipsta ( addr ip l{ip} -- )
	l_A ! a ! 
	dup s" AT+CIPSTA=" rot str-to-RAM
	h# 22 over put-char													( " )
	dup a get l_A get rot put-at-end
	h# 22 swap put-char													( " )
;

\ Builts a string for ask the IP of AP.
: build-qcipap ( addr -- )
	s" AT+CIPAP?" rot str-to-RAM
;

\ Builts a string for set the IP of AP.
: build-qcipsap ( addr ip l{ip} -- )
	l_A ! a ! 
	dup s" AT+CIPAP=" rot str-to-RAM
	h# 22 over put-char													( " )
	dup a get l_A get rot put-at-end
	h# 22 swap put-char													( " )
;

\ Builts a string for ask about the connection state.
: build-ecipstatus ( addr -- )
	s" AT+CIPSTATUS" rot str-to-RAM
;

\ Builts a string for establish TCP single connection.
: build-ecipstart-single ( addr ip l{ip} port l{port} -- )
	l_B ! b ! l_C ! c !
	dup s" AT+CIPSTART=" rot str-to-RAM
	h# 22 over put-char													( " )
	dup s" TCP" rot put-at-end
	h# 22 over put-char h# 2C over put-char						( " , )
	h# 22 over put-char													( " )
	dup c get l_C get rot put-at-end
	h# 22 over put-char h# 2C over put-char						( " , )
	b get l_B get rot put-at-end
;

\ Builts a string for establish TCP multiple connection.
: build-ecipstart-multiple ( addr id ip l{ip} port l{port} -- )
	l_B ! b ! l_C ! c !
	num2char swap dup s" AT+CIPSTART=" rot str-to-RAM						( id A A ) ( id A )
	swap over put-char h# 2C over put-char							( id, )
	h# 22 over put-char													( " )
	dup s" TCP" rot put-at-end
	h# 22 over put-char h# 2C over put-char						( " , )
	h# 22 over put-char													( " )
	dup c get l_C get rot put-at-end
	h# 22 over put-char h# 2C over put-char						( " , )
	b get l_B get rot put-at-end
;

: build-scipsend-single ( addr length l{length} -- )
	l_B ! b !
	dup s" AT+CIPSEND=" rot str-to-RAM
	b get l_B get rot put-at-end
;

: build-scipsend-multiple ( addr id length l{length} -- )
	l_B ! b ! num2char
	over s" AT+CIPSEND=" rot str-to-RAM
	over put-char h# 2C over put-char	
	b get l_B get rot put-at-end
;

: build-ecipclose-single ( addr -- )
	s" AT+CIPCLOSE" rot str-to-RAM
;

: build-ecipclose-multiple ( addr id -- )
	num2char swap dup s" AT+CIPCLOSE=" rot str-to-RAM
	put-char
;

: build-ecifsr ( addr -- )
	s" AT+CIFSR" rot str-to-RAM
;

: build-qcipmux ( addr -- )
	s" AT+CIPMUX?" rot str-to-RAM
;

: build-scipmux ( addr mode -- )
	num2char swap dup s" AT+CIPMUX=" rot str-to-RAM
	put-char
;

: build-scipserver-create ( addr port l{port} --  )
	l_A ! a !
	dup s" AT+CIPSERVER=1," rot str-to-RAM
	a get l_A get rot put-at-end
;

: build-scipserver-delete ( addr --  )
	s" AT+CIPSERVER=0" rot str-to-RAM
;
