\ Author: Alexis Cuero Losada
\ Email: alcuerol@gmail.com
\ Date: 2015-07-21
\ Version: 2.0

include util.fs

variable a
variable b
variable l_A
variable l_B
variable temp

: num2char ( n -- n+48 )
	d# 48 +
;

\ Returns the char of the position pos at the string. Out of range returns -1.
: char-at ( A pos -- c{addr[pos]} )
	2dup swap load-char <
	over h# 0 >=
	and if
		1+ + load-char
	else
		2drop d# -1
	then
;

\ Compares two string, if both are the same, flag = 1, else flag = 0.
: compare-str ( A B -- flag )
	2dup 1+ b ! 1+ a !
	load-char swap load-char dup l_A !								( l{B} A ) ( l{B} l{A} )
	= if
		h# 0 begin 															( c )
			dup dup a get + load-char									( c c c{a>} )
			swap b get + load-char										( c c{a>} c{b>} )
			= if
				1+ dup l_A get												( c++ c++ l{A} )
				= if																		
					drop h# 1 dup
				else
					h# 2
				then
			else
				drop h# 0 dup
			then
		h# 2 <> until
	else
		h# 0
	then
;

\ Compares two string, if both are the same, flag = 1, else flag = 0.
: compare ( A b l{B} -- flag )
	-rot b ! dup 1+ a !													( l{B} A b ) ( l{B} A a ) ( l{B} A )
	load-char 2dup l_A ! l_B !											( l{B} l{A} ) ( l{B} l{A} l{B} l{A} ) ( l{B} l{A} )
	= if
		h# 0 begin 															( c )
			dup dup a get + load-char									( c c c{a>} )
			swap b get + c@												( c c{a>} c{b>} )
			= if
				1+ dup l_A get												( c++ c++ l{A} )
				= if																		
					drop h# 1 dup
				else
					h# 2
				then
			else
				drop h# 0 dup
			then
		h# 2 <> until
	else
		h# 0
	then
;

\ Concatenates the string store since A, with the string store since B. Returns nothing.
: concat ( A B -- )
	2dup load-char l_B ! load-char l_A !							( A B A l{B} ) ( A B )
	1+ b ! 1+ a !
	h# 0 begin																( c )
		dup b get + load-char											( c b> ) ( c c{b>} )
		over l_A get a get + + save-char 1+							( c c{b>} c l{A} a ) ( c c{b>} a> ) ( c++ )
		dup l_B get
		= if
			l_A get + A get 1- save-char								( c+++l{A} A )
			h# 0
		else
			h# 1
		then
	0= until
;

\ Returns a flag, if the string store in addr1 contains the string store in addr2, flag = 1, else flag = 0.
: contains-str ( A B -- flag )
	over load-char over load-char										( A B l{A} l{B} )
	2dup l_B ! l_A !	
	> if																		( A B )
		1+ b ! 1+ a !
		h# 0 h# 0 begin													( c1 c2 )
			2dup b get + load-char swap a get + load-char		( c1 c2 c1 c{b>} ) ( c1 c2 c{b>} c{a>} )
			2swap swap 1+ swap 2swap									( c{b>} c{a>} c2 c1++ ) ( c1++ c2 c{b>} c{a>} )
			= if	
				1+	dup l_B get												( c1++ c2++ c2++ l{B} )
				= if															( c1++ c2++ )
					2drop h# 1 dup
				else
					over l_A get
					= if
						2drop h# 0 dup
					else
						h# 2
					then
				then
			else																( c1++ c2 )
				over l_A get												( c1++ c2 c1++ l{A} )
				= if
					2drop h# 0 dup
				else
					drop h# 0 h# 2
				then
			then
		h# 2 <> until
	else																		( A B )
		l_A get l_B get													( l{A} l{B} )
		= if
			compare-str
		else
			2drop h# 0
		then
	then
;

\ Returns a flag, if the string store in addr1 contains in addr2, flag = 1, else flag = 0.
: contains ( A b l_B -- flag )
	rot dup load-char rot												( A b l{B} ) ( b l{B} A l{A} ) ( b A l{A} l{B} )
	2swap swap 2swap														( l{A} l{B} b A ) ( l{A} l{B} A b ) ( A b l{A} l{B} )
	2dup l_B ! l_A !	
	> if																		( A b )
		b ! 1+ a !
		h# 0 h# 0 begin													( c1 c2 )
			2dup b get + c@ swap a get + load-char					( c1 c2 c1 c{b>} ) ( c1 c2 c{b>} c{a>} )
			2swap swap 1+ swap 2swap									( c{b>} c{a>} c2 c1++ ) ( c1++ c2 c{b>} c{a>} )
			= if	
				1+	dup l_B get												( c1++ c2++ c2++ l{B} )
				= if															( c1++ c2++ )
					2drop h# 1 dup
				else
					over l_A get
					= if
						2drop h# 0 dup
					else
						h# 2
					then
				then
			else																( c1++ c2 )
				over l_A get												( c1++ c2 c1++ l{A} )
				= if
					2drop h# 0 dup
				else
					drop h# 0 h# 2
				then
			then
		h# 2 <> until
	else																		( A b )
		l_A get l_B get													( l{A} l{B} )
		= if
			l_B get compare												( A b l{B} )
		else
			2drop h# 0
		then
	then
;

\ Returns the index of the first match where B is founded in A, It starts since p position, if it isn't founded or l{A} < l{B}, return -1.
: first-index-str ( A B start_pos -- first_index )
	-rot 2dup 1+ b ! 1+ a !												( p A B A B ) ( p A B A ) ( p A B )
	load-char swap load-char 2dup l_A ! l_B !						( p l{B} l{A} ) ( p l{B} l{A} l{B} l{A} ) ( p l{B} l{A} )
	dup 2swap 2dup + -rot 2swap swap									( l{A} l{A} p l{B} p+l{B} ) ( l{A} p l{B} l{A} p+l{B} ) ( l{A} p l{B} p+l{B} l{A} )
	> if																		( l{A} p l{B} )
		drop 2drop d# -1
	else
		rot swap																( p l{A} l{B} )
		> if																	( p )
			h# 0 begin														( c1 c2 )
				2dup b get + load-char swap a get + load-char	( c1 c2 c1 c{b>} ) ( c1 c2 c{b>} c{a>} )
				2swap swap 1+ swap 2swap								( c{b>} c{a>} c2 c1++ ) ( c1++ c2 c{b>} c{a>} )
				= if															( c1++ c2 )
					1+ dup l_B get											( c1++ c2++ c2++ l{B} )
					= if														( c1++ c2++ )
						drop l_B get -										( c1++-l{B} )
						h# 1
					else													
						over l_A get										( c1++ c2++ c1++ l{A} )
						= if													( c1++ c2++ )
							2drop d# -1 h# 0
						else
							h# 2
						then
					then
				else															( c1++ c2 )
					over l_A get											( c1++ c2 c1++ l{A} )
					= if														( c1++ c2 )
						2drop d# -1 h# 0
					else
						drop h# 0 h# 2
					then
				then
			h# 2 <> until
		else																	( p )
			drop a get 1- b get 1- compare-str						( compare{A, B} )
			h# 1 = if										
				h# 0
			else
				d# -1
			then
		then
	then
;

\ Returns the index of the first match where B is founded in A, It starts since p position, if it isn't founded or l{A} < l{B}, return -1.
: first-index ( A b l{B} start_pos -- first_index )
	swap l_B ! -rot b ! dup 1+ a !									( A b p l{B} ) ( p A b ) ( p A a ) ( p A )
	load-char l_A !														( p )
	l_B get 2dup + l_A get dup -rot									( p l{B} p l{B} ) ( p l{B} p+l{B} l{A} l{A} ) ( p l{B} l{A} p+l{B} l{A} )
	> if																		( p l{B} l{A} )
		drop 2drop d# -1
	else
		swap																	( p l{A} l{B} )
		> if																	( p )
			h# 0 begin														( c1 c2 )
				2dup b get + c@ swap a get + load-char				( c1 c2 c1 c{b>} ) ( c1 c2 c{b>} c{a>} )
				2swap swap 1+ swap 2swap								( c{b>} c{a>} c2 c1++ ) ( c1++ c2 c{b>} c{a>} )
				= if															( c1++ c2 )
					1+ dup l_B get											( c1++ c2++ c2++ l{B} )
					= if														( c1++ c2++ )
						drop l_B get -										( c1++-l{B} )
						h# 1
					else													
						over l_A get										( c1++ c2++ c1++ l{A} )
						= if													( c1++ c2++ )
							2drop d# -1 h# 0
						else
							h# 2
						then
					then
				else															( c1++ c2 )
					over l_A get											( c1++ c2 c1++ l{A} )
					= if														( c1++ c2 )
						2drop d# -1 h# 0
					else
						drop h# 0 h# 2
					then
				then
			h# 2 <> until
		else																	( p )
			drop a get 1- b get l_B get compare						( compare{A, b, l{B}} )
			h# 1 = if										
				h# 0
			else
				d# -1
			then
		then
	then
;

\ Returns the last of the first match where B is founded in A, The end is the p position, if it isn't founded, l{A} < l{B} or p >= l{A}, return -1.
: last-index-str ( A B pos -- last_index )
	-rot 2dup 1+ b ! 1+ a !												( p A B A B ) ( p A B A ) ( p A B )
	load-char l_B ! load-char l_A !									( p A l{B} ) ( p )
	dup l_B get 1- >= over l_A get <									( p p>=l{B}-1 ) ( p p>=l{B}-1 p<l{A} )
	and if																	( p )
		dup h# AA save-char
		l_A get l_B get													( p l{A} l{B} )
		> if																	( p )
			l_B get 1- begin												( c1 c2 )
				2dup b get + load-char swap a get + load-char	( c1 c2 c1 c{b>} ) ( c1 c2 c{b>} c{a>} )
				2swap swap 1- swap 2swap								( c{b>} c{a>} c2 c1-- ) ( c1-- c2 c{b>} c{a>} ) ( --> Very expensive )
				= if															( c1-- c2 )
					1- dup													( c1-- c2-- c2-- )
					0< if														( c1-- c2-- )
						drop 1+ h# 1
					else
						over													( c1-- c2-- c1-- )
						0< if													( c1-- c2-- )
							2drop d# -1 h# 0
						else
							h# 2
						then
					then
				else															( c1-- c2 )
					over														( c1-- c2 c1-- )
					0< if														( c1-- c2 )
						2drop d# -1 h# 0
					else
						drop l_B get 1- h# 2
					then
				then
			h# 2 <> until
		else
			drop a get 1- b get 1- compare-str							( compare{A, B} )
			h# 1 = if										
				h# 0
			else
				d# -1
			then
		then
	else
		drop d# -1
	then
;

\ Returns the last of the first match where B is founded in A, The end is the p position, if it isn't founded, l{A} < l{B} or p >= l{A}, return -1.
: last-index ( A b l{B} pos -- last_index )
	swap l_B ! -rot b ! dup 1+ a !									( A b p l{B} ) ( p A b ) ( p A a) ( p A )
	load-char l_A !														( p l{A} ) ( p )
	dup l_B get 1- >= over l_A get <									( p p l{B} ) ( p p>=l{B}-1 ) ( p p>=l{B}-1 p<l{A} )
	and if																	( p )
		dup h# AA save-char
		l_A get l_B get													( p l{A} l{B} )
		> if																	( p )
			l_B get 1- begin												( c1 c2 )
				2dup b get + c@ swap a get + load-char				( c1 c2 c1 c{b>} ) ( c1 c2 c{b>} c{a>} )
				2swap swap 1- swap 2swap								( c{b>} c{a>} c2 c1-- ) ( c1-- c2 c{b>} c{a>} ) ( --> Very expensive )
				= if															( c1-- c2 )
					1- dup													( c1-- c2-- c2-- )
					0< if														( c1-- c2-- )
						drop 1+ h# 1
					else
						over													( c1-- c2-- c1-- )
						0< if													( c1-- c2-- )
							2drop d# -1 h# 0
						else
							h# 2
						then
					then
				else															( c1-- c2 )
					over														( c1-- c2 c1-- )
					0< if														( c1-- c2 )
						2drop d# -1 h# 0
					else
						drop l_B get 1- h# 2
					then
				then
			h# 2 <> until
		else
			drop a get 1- b get l_B get compare							( compare{A, b, l{B} )
			h# 1 = if										
				h# 0
			else
				d# -1
			then
		then
	else
		drop d# -1
	then
;

\ Gets a substring since start to end of A and save this in B.
: substring ( A B start end -- flag )
	2swap 2dup load-char l_B ! load-char l_A !					( s e A B A l{B} ) ( s e A B l{A} ) ( s e A B )
	1+ b ! rot dup rot 1+ + a ! dup temp !							( s e A ) ( e s s A ) ( e s a+s ) ( e s )
	h# 0 >= over l_A get <												( e s>=0 ) ( e s>=0 e<l{A} )
	and if																	( e )
		h# 0 begin															( e c )
			dup dup a get + load-char swap b get +					( e c c a> ) ( e c c{a>} b> )
			rot 1+ -rot	save-char										( e c++ c{a>} b> ) ( e c++ )
			2dup temp get + swap											( e c++ e c++ s ) ( e c++ c+++s e )
			> if																( e c++ )
				b get 1- save-char										( e c++ B ) ( e )
				drop h# 1 dup
			else																( e c++ )
				h# 2
			then
		h# 2 <> until
	else																		( e )
		drop d# -1
	then
;

\ Puts a char at the end of a string stored in A addres in strRAM.
: put-char ( char A -- )
	dup load-char 1+ 2dup swap save-char							( char A l{A}+1 )	( char A l{A}+1 A l{A}+1 ) ( char A l{A}+1 l{A}+1 A ) ( char A l{A}+1 )
	+ save-char																( char a+l{A} ) (  )
;

\ Puts a string b at the end of a string stored in A addres in strRAM.
: put-at-end ( b l{B} A -- )
	2dup load-char dup l_A ! + over save-char						( b l{B} A l{B} A ) ( b l{B} A l{B} l{A} l{A} ) ( b l{B} A l{B}+l{A} A ) ( b l{B} A )
	-rot l_B ! b !															( A b l{B} ) ( A )
	l_A get 1+ +															( A l{A}+1 ) ( a| )
	h# 0 begin																( a| c )
		2dup + over b get + c@ swap save-char						( a| c a> ) ( a| c a> b> ) ( a| c c{b>} a> ) ( a| c )
		1+ dup l_B get														( a| c++ c++ l{B} )
		= if																	( a| c++ )
			2drop h# 1
		else
			h# 2
		then
	h# 2 <> until
;
